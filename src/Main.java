
import VueControleur.VueControleurPacMan;
import modele.Jeu;

public class Main {
    public static void main(String[] args) {
        
        Jeu jeu = new Jeu();
        
        VueControleurPacMan vc = new VueControleurPacMan(Jeu.SIZE_X, Jeu.SIZE_Y);
        
        jeu.addObserver(vc);
        vc.setJeu(jeu);
        
        vc.setVisible(true);
        
        jeu.start();
    }
}
