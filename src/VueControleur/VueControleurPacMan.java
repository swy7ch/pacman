package VueControleur;

import java.awt.*;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import modele.*;


/** Cette classe a deux fonctions :
 *  (1) Vue : proposer une représentation graphique de l'application (cases graphiques, etc.)
 *  (2) Controleur : écouter les évènements clavier et déclencher le traitement adapté sur le modèle (flèches direction Pacman, etc.))
 *
 */
public class VueControleurPacMan extends JFrame implements Observer {

    private Jeu jeu; // référence sur une classe de modèle : permet d'accéder aux données du modèle pour le rafraichissement, permet de communiquer les actions clavier (ou souris)

    private int sizeX; // taille de la grille affichée
    private int sizeY;

    private ImageIcon icoPacMan_d; // icones affichées dans la grille
    private ImageIcon icoPacMan_h;
    private ImageIcon icoPacMan_g;
    private ImageIcon icoPacMan_b;
    private ImageIcon icoFantomeRed;
    private ImageIcon icoFantomePink;
    private ImageIcon icoFantomeOrange;
    private ImageIcon icoFantomeBlue;
    private ImageIcon icoFantomeWeak;
    private ImageIcon icoCouloir;
    private ImageIcon icoMur;
    private ImageIcon icoPacGum;
    private ImageIcon icoSuperPacGum;
    private ImageIcon icoPorte;
    private ImageIcon icoHeart;
    private ImageIcon icoEmptyHeart;

    private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associé à une icône, suivant ce qui est présent dans la partie modèle)
    private JComponent grilleJLabels; // Les différentqs composants graphiques à implémenter
    private JPanel stats;
    private JLabel scoreBox;
    private JPanel health;
    private JLabel[] lifeBox;
    private JPanel gamescreen;
    private JLabel levelBox;
    private JProgressBar invincibilityBar;

    public VueControleurPacMan(int _sizeX, int _sizeY) {

        sizeX = _sizeX;
        sizeY = _sizeY;

        chargerLesIcones();
        placerLesComposantsGraphiques();

        ajouterEcouteurClavier();

    }

    private void ajouterEcouteurClavier() {

        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                
                switch(e.getKeyCode()) {  // on écoute les flèches de direction du clavier
                    case KeyEvent.VK_LEFT : jeu.getPacman().setDirection(Direction.gauche); break;
                    case KeyEvent.VK_RIGHT : jeu.getPacman().setDirection(Direction.droite); break;
                    case KeyEvent.VK_DOWN : jeu.getPacman().setDirection(Direction.bas); break;
                    case KeyEvent.VK_UP : jeu.getPacman().setDirection(Direction.haut); break;
                    case 27 : jeu.setEtat(EtatJeu.EnPause); break; // touche echap
                }
                
            }

        });

    }

    public void setJeu(Jeu _jeu) {
        jeu = _jeu;
    }

    private void chargerLesIcones() {
        icoPacMan_d = chargerIcone("Images/Pacman_d.png");
        icoPacMan_h = chargerIcone("Images/Pacman_h.png");
        icoPacMan_g = chargerIcone("Images/Pacman_g.png");
        icoPacMan_b = chargerIcone("Images/Pacman_b.png");
        icoCouloir = chargerIcone("Images/Couloir.png");
        icoFantomeRed = chargerIcone("Images/red.png");
        icoFantomePink = chargerIcone("Images/pink.png");
        icoFantomeOrange = chargerIcone("Images/orange.png");
        icoFantomeBlue = chargerIcone("Images/blue.png");
        icoFantomeWeak = chargerIcone("Images/Fantome.png");
        icoMur = chargerIcone("Images/Mur.png");
        icoPacGum = chargerIcone("Images/PacGum.png");
        icoPorte = chargerIcone("Images/Porte.png");
        icoSuperPacGum = chargerIcone("Images/SuperPacGum.png");
        icoHeart = chargerIcone("Images/full_heart.png");
        icoEmptyHeart = chargerIcone("Images/empty_heart.png");
    }

    private ImageIcon chargerIcone(String urlIcone) {

        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleurPacMan.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return new ImageIcon(image);
    }

    private void placerLesComposantsGraphiques() {

        setTitle("PacMan");
        setSize(20*sizeX + 50, 25*sizeY +10);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // permet de terminer l'application à la fermeture de la fenêtre
        setLayout(new BorderLayout());

        gamescreen = new JPanel();
        gamescreen.setBackground(Color.BLACK);
        gamescreen.setSize(20*sizeX + 10, 25*sizeY +10);

        
        grilleJLabels = new JPanel(new GridLayout(sizeX, sizeY)); // grilleJLabels va contenir les cases graphiques et les positionner sous la forme d'une grille
        grilleJLabels.setBackground(Color.BLACK);

        tabJLabel = new JLabel[sizeX][sizeY];

        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {

                JLabel jlab = new JLabel();
                tabJLabel[x][y] = jlab; // on conserve les cases graphiques dans tabJLabel pour avoir un accès pratique à celles-ci (voir mettreAJourAffichage() )
                grilleJLabels.add(jlab);
                
            }

        }
        stats = new JPanel(new GridLayout(1,4,4,4));
        stats.setBorder(new EmptyBorder(10,10,10,10));
        stats.setBackground(Color.black);
        levelBox = new JLabel();
        levelBox.setForeground(Color.yellow);
        levelBox.setText("Niveau 1");
        scoreBox = new JLabel("score : 0");
        scoreBox.setForeground(Color.yellow);
        lifeBox = new JLabel[3];
        health = new JPanel();
        JLabel healthText = new JLabel("health: ");
        health.add(healthText);
        healthText.setForeground(Color.yellow);
        health.setBackground(Color.black);
        invincibilityBar = new JProgressBar(0, 30);
        invincibilityBar.setForeground(Color.yellow);
        invincibilityBar.setBackground(Color.BLACK);
        
        for(int i = 0; i < 3; i++) { // Affichage de coeurs pleins ou vide en fonction des vies restantes de Pacman
        	lifeBox[i] = new JLabel();
        	lifeBox[i].setIcon(icoHeart);
        	lifeBox[i].setForeground(Color.black);
        	health.add(lifeBox[i]);
        }

        stats.add(levelBox);
        stats.add(scoreBox);
        stats.add(invincibilityBar);
        stats.add(health);

        gamescreen.add(BorderLayout.CENTER,grilleJLabels);
        gamescreen.add(BorderLayout.SOUTH,stats);
        add(gamescreen);
    }

    
    /**
     * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté de la vue (tabJLabel)
     */
    private void mettreAJourAffichage() {


        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                if (jeu.getGrille()[x][y] instanceof Pacman) { // si la grille du modèle contient un Pacman, on associe l'icône Pacman du côté de la vue
                	switch(jeu.getPacman().getDirection()) {
                	case droite:
                		tabJLabel[x][y].setIcon(icoPacMan_d);
                		break;
                	case haut:        
                		tabJLabel[x][y].setIcon(icoPacMan_h);
                		break;
                	case gauche:        
                		tabJLabel[x][y].setIcon(icoPacMan_g);
                		break;
                	case bas:        
                		tabJLabel[x][y].setIcon(icoPacMan_b);
                		break;
                	}
                } else if (jeu.getGrille()[x][y] instanceof Fantome) {
                    switch (((Fantome)jeu.getGrille()[x][y]).getColors()){ //apparence du fantome différent en fonction de sa couleur
                        case Red:
                            if(jeu.isPacmanBoosted()){
                                tabJLabel[x][y].setIcon(icoFantomeWeak);
                            }
                            else {
                                tabJLabel[x][y].setIcon(icoFantomeRed);
                            }
                            break;

                        case Blue:
                            if(jeu.isPacmanBoosted()){
                                tabJLabel[x][y].setIcon(icoFantomeWeak);
                            }
                            else {
                                tabJLabel[x][y].setIcon(icoFantomeBlue);
                            }
                            break;

                        case Pink:
                            if(jeu.isPacmanBoosted()){
                                tabJLabel[x][y].setIcon(icoFantomeWeak);
                            }
                            else {
                                tabJLabel[x][y].setIcon(icoFantomePink);
                            }
                            break;

                        case Orange:
                            if(jeu.isPacmanBoosted()){
                                tabJLabel[x][y].setIcon(icoFantomeWeak);
                            }
                            else {
                                tabJLabel[x][y].setIcon(icoFantomeOrange);
                            }
                            break;
                    }
                } else if (jeu.getGrille()[x][y] instanceof Mur){
                    tabJLabel[x][y].setIcon(icoMur);
                }  else if (jeu.getGrille()[x][y] instanceof Couloir){
                	if(jeu.getGrille()[x][y].hasPacGum()) {
                	    if(((Couloir) jeu.getGrille()[x][y]).isSuperPacGum()){
                            tabJLabel[x][y].setIcon(icoSuperPacGum);
                        }
                	    else{
                            tabJLabel[x][y].setIcon(icoPacGum);
                        }
                	} else {
                		tabJLabel[x][y].setIcon(icoCouloir);
                	}
                }
                else if (jeu.getGrille()[x][y] instanceof Porte){
                    tabJLabel[x][y].setIcon(icoPorte);
                }
                else{
                    tabJLabel[x][y].setIcon(icoCouloir);
                }
            }
        }
        scoreBox.setText("score : " + jeu.getScore());
        levelBox.setText("Niveau " + jeu.getNiveauActuel());
        invincibilityBar.setValue(jeu.durationBoostPacman);
        int health = jeu.getLife();
        switch(health) { // Actualisation des vies restantes
        case 2:
        	lifeBox[2].setIcon(icoEmptyHeart);
        	break;
        case 1:
        	lifeBox[1].setIcon(icoEmptyHeart);
        	break;
        case 0:
        	lifeBox[0].setIcon(icoEmptyHeart);
        	break;
        }
    }

    /** Crétion du nouvel écran en fonction de l'état de la partie
     */
    public JOptionPane affichageNouvelEcran(EtatJeu etat) {
        JOptionPane endScreen = new JOptionPane();
        switch (etat){
            case EnPause:{
                String[] listChoix = {"Continuer", "Quitter"};
                int rang = JOptionPane.showOptionDialog(null, "Votre score est de : "+jeu.getPacman().getScore(), "Pause", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,null,listChoix, null);
                if(rang == 0){
                    mettreAJourAffichage();
                    jeu.setEtat(EtatJeu.EnJeu);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    jeu.isRunning = false;
                    dispose();
                }
                break;
            }

            case PerduVie: {
                int option = JOptionPane.showConfirmDialog(null, "Vous avez perdu une vie, voulez vous continuer ?", "Oh non", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(option == JOptionPane.OK_OPTION){
                    jeu.resetPositions();
                    mettreAJourAffichage();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    jeu.isRunning = false;
                    dispose();
                }
                break;
            }

            case FinDeNiveau: {
                String[] listChoix = {"Niveau suivant", "Quitter"};
                int rang = JOptionPane.showOptionDialog(null, "Vous avez fini le niveau "+jeu.getNiveauActuel(), "Félicitations", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,null,listChoix, null);
                if(rang == 0){
                    jeu.resetLevel();
                    mettreAJourAffichage();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    jeu.isRunning = false;
                    dispose();
                }
                break;
            }

            case GameOver: {
                jeu.isRunning = false;
                GameOver model_ecran_fin = new GameOver(jeu);
                setContentPane(model_ecran_fin.$$$getRootComponent$$$());
                setVisible(true);
                break;
               }
        }

    	return endScreen;

    }

    @Override
    public void update(Observable o, Object arg) { //Mise à jour de l'écran
        
        if(jeu.getEtat() != EtatJeu.EnJeu){
            affichageNouvelEcran(jeu.getEtat());
        }
        else{
            mettreAJourAffichage();
        }
        
    }

}
