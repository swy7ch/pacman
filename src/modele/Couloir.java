package modele;

public class Couloir extends Entite{
	
	public PacGum pacgum;
	private boolean isSuper;

	
	public Couloir(Jeu _jeu, boolean _pacgum, boolean _isSuperpacgum) {
		super(_jeu);
		if (_pacgum) {
			pacgum = new PacGum(_jeu);
			isSuper = _isSuperpacgum;
		}
	}

	@Override
	public void choixDirection() {
		// TODO Auto-generated method stub
		d = Direction.aucune;
	}
	
	public boolean hasPacGum() {
		return pacgum != null;
	}

	public boolean isSuperPacGum() {
		return isSuper;
	}
	
	public void destroyPacGum() {
		pacgum = null;
	}


}
