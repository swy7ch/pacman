package modele;

public abstract class Entite implements Runnable {

    protected Jeu jeu;
    protected Direction d;
    
    public abstract void choixDirection(); // stratégie de déclassement définie dans les sous-classes, concernant Pacman, ce sont les évènements clavier qui définissent la direction
    
    public void avancerDirectionChoisie() {
        jeu.deplacerEntite(this, d);
    }
    
    
    public Entite(Jeu _jeu) {
        jeu = _jeu;
    }
    
    
    
    @Override
    public void run() {
        choixDirection();
        avancerDirectionChoisie();
    }
    
    public void scoreup(int bonus) {    };
    public boolean hasPacGum() {
    	return false;
    };
    public void destroyPacGum() {     };
    
    public final Direction getDirection() {
    	return d;
    }
    
    public Direction getOppositeDirection(Direction d){
        switch (d){
            case droite: return Direction.gauche;

            case gauche: return Direction.droite;

            case haut: return Direction.bas;

            case bas: return Direction.haut;
        }
        return Direction.aucune;
    }
    
}
