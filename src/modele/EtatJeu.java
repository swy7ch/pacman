package modele;

/** Type énuméré des états possibles du jeu **/

public enum EtatJeu {
    EnJeu,
    EnPause,
    PerduVie,
    GameOver,
    FinDeNiveau
}
