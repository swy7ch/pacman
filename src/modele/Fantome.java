package modele;

import java.util.Random;

public class Fantome extends Entite {

    private Random r = new Random();
    private Colors color;
    private boolean isInJail = false;
    private int timeLeftInJail = 0;

    public Fantome(Jeu _jeu, Colors colors) {
        super(_jeu);
        d = Direction.droite;
        this.color = colors;

    }

    public Colors getColors(){return color;}

    @Override
    public void choixDirection() {
        if(jeu.regarderDansLaDirection(this, d) instanceof Fantome){ //Un fantome se déplace dans la direction opposée si il entre en contact avec un de ses congénères
            d = getOppositeDirection(d);
        }
        if(jeu.regarderDansLaDirection(this, d) instanceof Mur || jeu.isOnCrossing(this)) { //Change de direction si il se déplace vers un mur ou si il est sur un croisement
            Direction newDirection = d;
            while(newDirection == d || newDirection == getOppositeDirection(d)) {
                switch (r.nextInt(4)) {
                    case 0:
                        newDirection = Direction.droite;
                        break;
                    case 1:
                        newDirection = Direction.bas;
                        break;
                    case 2:
                        newDirection = Direction.gauche;
                        break;
                    case 3:
                        newDirection = Direction.haut;
                        break;
                }
            }
            d = newDirection;
        }
    }

    public boolean isInJail() {
        return isInJail;
    }

    public void setInJail(boolean inJail) {
        isInJail = inJail;
        timeLeftInJail = 25;
    }

    public void setTimeLeftInJail(int timeLeftInJail) {
        this.timeLeftInJail = timeLeftInJail;
    }

    public boolean decrementTimeJail(){
        this.timeLeftInJail--;
        if(timeLeftInJail <=0){
            isInJail = false;
            return true;
        }
        else{
            return false;
        }
    }
}
