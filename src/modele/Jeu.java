package modele;

import java.awt.Point;
import java.util.HashMap;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/** La classe Jeu a deux fonctions 
 *  (1) Gérer les aspects du jeu : condition de défaite, victoire, nombre de vies
 *  (2) Gérer les coordonnées des entités du monde : déplacements, collisions, perception des entités, ... 
 *
 */
public class Jeu extends Observable implements Runnable {

    public static final int SIZE_X = 30;
    public static final int SIZE_Y = 28;

    private static final int NP_X = 18; //position départ pacman
    private static final int NP_Y = 13;

    private static final int FR_X = 11; //position départ fantome rouge
    private static final int FR_Y = 11;

    private static final int FP_X = 13; //position départ fantome rose
    private static final int FP_Y = 12;

    private static final int FO_X = 13; //position départ fantome Orange
    private static final int FO_Y = 13;

    private static final int FB_X = 13; //position départ fantome bleu
    private static final int FB_Y = 14;

    private static final Point Exit = new Point(11,14); //position sortie fantome

    public int durationBoostPacman = 0;
    private boolean isPacmanBoosted = false;
    private int niveauActuel = 1;
    private int numberOfPacGum = 0;

    private Pacman pm;
    private Fantome fr;
    private Fantome fp;
    private Fantome fb;
    private Fantome fo;
    
    public boolean isRunning = true;
    private EtatJeu etat = EtatJeu.EnJeu;

    private HashMap<Entite, Point> map = new  HashMap<Entite, Point>(); // permet de récupérer la position d'une entité à partir de sa référence
    private Entite[][] grilleEntites = new Entite[SIZE_X][SIZE_Y]; // permet de récupérer une entité à partir de ses coordonnées

    private int[][] MapOriginal ={  //1 = Mur, 0 = Couloir, 9 = couloir sans pacGum, 2 et 3 = points pour se téléporter, 8 = porte des fantomes, 4 = SuperPacGum, 5 = Croisement
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,0,0,0,0,0,5,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1},
            {1,4,1,9,9,1,0,1,9,9,9,1,0,1,1,0,1,9,9,9,1,0,1,9,9,1,4,1},
            {1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1},
            {1,5,0,0,0,0,5,0,0,5,0,0,5,0,0,5,0,0,5,0,0,5,0,0,0,0,5,1},
            {1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,0,1},
            {1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,0,1},
            {1,0,0,0,0,0,5,1,1,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1},
            {9,9,9,9,9,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,9,9,9,9,9},
            {9,9,9,9,9,1,0,1,1,0,0,0,5,0,0,5,0,0,0,1,1,0,1,9,9,9,9,9},
            {9,9,9,9,9,1,0,1,1,0,1,1,1,8,8,1,1,1,0,1,1,0,1,9,9,9,9,9},
            {1,1,1,1,1,1,0,1,1,0,1,9,9,9,9,9,9,1,0,1,1,0,1,1,1,1,1,1},
            {2,9,9,9,9,9,5,0,0,0,1,9,9,9,9,9,9,1,0,0,0,5,9,9,9,9,9,3},
            {1,1,1,1,1,1,0,1,1,0,1,9,9,9,9,9,9,1,0,1,1,0,1,1,1,1,1,1},
            {9,9,9,9,9,1,0,1,1,0,1,9,9,9,9,9,9,1,0,1,1,0,1,9,9,9,9,9},
            {9,9,9,9,9,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,9,9,9,9,9},
            {9,9,9,9,9,1,0,1,1,5,0,0,0,0,0,0,0,0,5,1,1,0,1,9,9,9,9,9},
            {1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1},
            {1,0,0,0,0,0,5,0,0,0,0,0,0,1,1,0,0,0,0,0,0,5,0,0,0,0,0,1},
            {1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1},
            {1,0,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,0,1},
            {1,4,0,0,1,1,5,0,0,5,0,0,5,0,0,5,0,0,5,0,0,5,1,1,0,0,4,1},
            {1,1,1,0,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,1,0,1,1,1},
            {1,0,0,5,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,5,0,0,1},
            {1,0,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,0,1},
            {1,0,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,1,1,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,5,0,0,5,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };

    public Jeu() {
        initialisationDesEntites();
    }
    
    public Entite[][] getGrille() {
        return grilleEntites;
    }
    
    public Pacman getPacman() {
        return pm;
    }
    
    private void creationPlateau(int[][] grille) { //Création du plateau par rapport à la grille donnée
        int hauteur = grille.length;
        int longueur = grille[0].length;
        for(int y = 0; y<longueur; y++) { //lignes
            for(int x = 0; x<hauteur;x++) { //colonnes
                switch(grille[x][y]){
                    case 1:{
                        Mur mur = new Mur(this);
                        grilleEntites[x][y] = mur;
                        map.put(mur, new Point(x,y));
                        break;
                    }
                    
                    case 2:
                    case 3:
                    case 9: {
                        Couloir couloir = new Couloir(this, false, false);
                        grilleEntites[x][y] = couloir;
                        map.put(couloir, new Point(x, y));
                        break;
                    }

                    case 8:{
                        Porte porte = new Porte(this);
                        grilleEntites[x][y] = porte;
                        map.put(porte, new Point(x,y));
                        break;
                    }

                    case 4: {
                        Couloir couloir = new Couloir(this, true, true);
                        grilleEntites[x][y] = couloir;
                        map.put(couloir, new Point(x, y));
                        numberOfPacGum++;
                        break;
                    }

                    case 0:
                    case 5: {
                        Couloir couloir = new Couloir(this, true, false);
                        grilleEntites[x][y] = couloir;
                        map.put(couloir, new Point(x, y));
                        numberOfPacGum++;
                        break;
                    }
                }
            }
        }
    }
    
    private void initialisationDesEntites() { //Placement de Pacman et des fantomes au départ d'une partie
        creationPlateau(MapOriginal);

        pm = new Pacman(this);
        grilleEntites[NP_X][NP_Y] = pm;
        map.put(pm, new Point(NP_X, NP_Y));

        fr = new Fantome(this, Colors.Red);
        grilleEntites[FR_X][FR_Y] = fr;
        map.put(fr, new Point(FR_X, FR_Y));

        fp = new Fantome(this, Colors.Pink);
        fp.setInJail(true);
        fp.setTimeLeftInJail(10);
        grilleEntites[FP_X][FP_Y] = fp;
        map.put(fp, new Point(FP_X, FP_Y));

        fo = new Fantome(this, Colors.Orange);
        fo.setInJail(true);
        fo.setTimeLeftInJail(15);
        grilleEntites[FO_X][FO_Y] = fo;
        map.put(fo, new Point(FO_X, FO_Y));

        fb = new Fantome(this, Colors.Blue);
        fb.setInJail(true);
        fb.setTimeLeftInJail(20);
        grilleEntites[FB_X][FB_Y] = fb;
        map.put(fb, new Point(FB_X, FB_Y));
        
    }
    
    
    /** Permet a une entité  de percevoir sont environnement proche et de définir sa strétégie de déplacement 
     * (fonctionalité utilisée dans choixDirection() de Fantôme)
     */
    public Object regarderDansLaDirection(Entite e, Direction d) {
        Point positionEntite = map.get(e);
        return objetALaPosition(calculerPointCible(positionEntite, d));
    }

    public boolean isOnCrossing(Entite e){ //Vérifie si l'entité est sur un point de croisement entre plusieurs couloirs (Utile aux déplacemetns des fantomes)
        Point posEntite = map.get(e);
        int pos = MapOriginal[posEntite.x][posEntite.y];
        if(pos == 5){
            return true;
        }
        else{
            return false;
        }

    }

    public Point positionEntite(Entite e) {
        return map.get(e);
    }

    /**
     * Remet Pacman et les fantômes à leurs positions initiales
     */
    public void resetPositions() {
    	Point pos_PM = positionEntite(pm);
    	deplacerEntite(pos_PM, new Point(NP_X, NP_Y), pm);
    	
    	Point pos_FR = positionEntite(fr);
    	deplacerEntite(pos_FR, new Point(FR_X, FR_Y), fr);
    	
    	Point pos_FP = positionEntite(fp);
    	fp.setInJail(true);
        fp.setTimeLeftInJail(20);
    	deplacerEntite(pos_FP, new Point(FP_X, FP_Y), fp);

    	Point pos_FB = positionEntite(fb);
    	fb.setInJail(true);
        fb.setTimeLeftInJail(25);
    	deplacerEntite(pos_FB, new Point(FB_X, FB_Y), fb);
    	
    	Point pos_FO = positionEntite(fo);
    	fo.setInJail(true);
        fo.setTimeLeftInJail(15);
    	deplacerEntite(pos_FO, new Point(FO_X, FO_Y), fo);

        etat = EtatJeu.EnJeu;
        isRunning = true;
    }

    /**
     * Remet le niveau à zéro
     */
     public void resetLevel(){
        map.clear();
        niveauActuel++;
        creationPlateau(MapOriginal);

         grilleEntites[NP_X][NP_Y] = pm;
         map.put(pm, new Point(NP_X, NP_Y));

         grilleEntites[FR_X][FR_Y] = fr;
         map.put(fr, new Point(FR_X, FR_Y));

         fp.setInJail(true);
         fp.setTimeLeftInJail(10);
         grilleEntites[FP_X][FP_Y] = fp;
         map.put(fp, new Point(FP_X, FP_Y));

         fo.setInJail(true);
         fo.setTimeLeftInJail(15);
         grilleEntites[FO_X][FO_Y] = fo;
         map.put(fo, new Point(FO_X, FO_Y));

         fb.setInJail(true);
         fb.setTimeLeftInJail(20);
         grilleEntites[FB_X][FB_Y] = fb;
         map.put(fb, new Point(FB_X, FB_Y));

        etat = EtatJeu.EnJeu;
     }
    
    /** Si le déclacement de l'entité est autorisé (pas de mur ou autre entité), il est réalisé
     */
    public void deplacerEntite(Entite e, Direction d) {

        Point pCourant = map.get(e);
        
        Point pCible = calculerPointCible(pCourant, d);
        
        if(e instanceof Pacman || e instanceof Fantome) {
        	if(pCible.x == 14) { //sur la ligne de téléportation
               	if(pCible.y == -1) { //sur le téléporteur de gauche, veut aller à gauche
               		pCible = new Point(14, 27); //shazam
               	} else if(pCible.y == 28) { //téléproteur de droite, veut aller à droite
               		pCible = new Point(14, 0); //shazam
               	}
        	}
        }
        
        if (contenuDansGrille(pCible)) {
        	
        	if (objetALaPosition(pCible) instanceof Couloir) {
        
        		if (e instanceof Pacman && objetALaPosition(pCible).hasPacGum()) { //Gain de points lors de l'avalement d'un pacgum
                    e.scoreup(5);
                    if(((Couloir) objetALaPosition(pCible)).isSuperPacGum()){ // Obtention de l'invincibilité si c'est un Super pacgum
                        pm.setSuperPacgum(true);
                        durationBoostPacman = 30;
                    }
        		}
        		deplacerEntite(pCourant, pCible, e);
        	}
        	
        	else if(e instanceof Pacman && (objetALaPosition(pCible) instanceof Fantome)) { //Gestion de collision si Pacman se déplace vers un fantome
        	    if(!(pm.getSuperPacgum()) && !pm.isAlreadyHit()){ // Vérification si Pacman est invincible ou si il a déja été touché
        	        pm.setAlreadyHit(true);
                    pm.die();
        	    	if(pm.getLife() <= 0) { //Vérification si c'est la dernière vie de Pacman
        	    		isRunning = false;
        	    		etat = EtatJeu.GameOver;
        	    	} else {
                        isRunning = false;
        	    	    etat = EtatJeu.PerduVie;
        	    	}
                }
        	    else{ //Téléportation du fantome en prison du a l'invincibilité de pacman
                    Point jail = new Point(13,13);
                    ((Fantome) objetALaPosition(pCible)).setInJail(true);
                    deplacerEntite(pCible, jail, objetALaPosition(pCible));
                }

        	}
        	else if (e instanceof Fantome && objetALaPosition(pCible) instanceof Pacman ){ //Gestion de collision quand un fantome se déplace vers Pacman
        	    if(!(pm.getSuperPacgum()) && !pm.isAlreadyHit()){
                    pm.setAlreadyHit(true);
                    pm.die();
        	    	if(pm.getLife() <=0) {
                        isRunning = false;
                        etat = EtatJeu.GameOver;
        	    	} else {
                        isRunning = false;
                        etat = EtatJeu.PerduVie;
        	    	}
                }
        	    else{
                    Point jail = new Point(13,13);
                    ((Fantome) e).setInJail(true);
                    deplacerEntite(pCible, jail, e);
                }
            }
        }
    }

    /**Calcul du point ou se déplace une entité en fonction de sa direction
     */
    
    private Point calculerPointCible(Point pCourant, Direction d) {
        Point pCible = null;
        
        switch(d) {
            case haut: pCible = new Point(pCourant.x-1, pCourant.y); break;
            case bas : pCible = new Point(pCourant.x+1, pCourant.y); break;
            case gauche : pCible = new Point(pCourant.x, pCourant.y-1); break;
            case droite : pCible = new Point(pCourant.x, pCourant.y+1); break;
            case aucune : pCible = pCourant;
            
        }
        
        return pCible;
    }

    /** Fonction de déplacement d'une entité d'un point A à un point B
     */
    
    private void deplacerEntite(Point pCourant, Point pCible, Entite e) {
        grilleEntites[pCourant.x][pCourant.y] = new Couloir(this, false, false);

        if(e instanceof Pacman && grilleEntites[pCible.x][pCible.y].hasPacGum()) {
            grilleEntites[pCible.x][pCible.y].destroyPacGum();
            numberOfPacGum--;
        }
        grilleEntites[pCible.x][pCible.y] = e;
        map.put(e, pCible);
    }

    /** Fait sortir un fantôme de la prison
     */
    private void exitJail(Fantome f){
        Point pCourant = map.get(f);
        Object contenuExit = grilleEntites[Exit.x][Exit.y];
        int i = 0;
        while(contenuExit instanceof Fantome || contenuExit instanceof Pacman || contenuExit instanceof Mur){
            i++;
            contenuExit = grilleEntites[Exit.x][Exit.y+i];
        }
        deplacerEntite(pCourant, new Point(Exit.x, Exit.y+i), f);
    }
    
    /** Vérifie que p est contenu dans la grille
     */
    private boolean contenuDansGrille(Point p) {
        return p.x >= 0 && p.x < SIZE_X && p.y >= 0 && p.y < SIZE_Y;
    }
    
    private Entite objetALaPosition(Point p) {
        Entite retour = null;
        
        if (contenuDansGrille(p)) {
            retour = grilleEntites[p.x][p.y];
        }
        
        return retour;
    }
    
    public int getScore() {
    	return getPacman().getScore();
    }
    
    public int getLife() {
    	return getPacman().getLife();
    }
    
    /**
     * Un processus est créé et lancé, celui-ci execute la fonction run()
     */
    public void start() {

        new Thread(this).start();

    }
    
    @Override
    public void run() {

        while (isRunning) {
        	if(etat == EtatJeu.EnJeu) {
                pm.setAlreadyHit(false);
        		for (Entite e : map.keySet()) { // déclenchement de l'activité des entités, map.keySet() correspond à la liste des entités
                    e.run();
                    if (e instanceof Pacman && ((Pacman) e).getSuperPacgum()){ //Actualisation de la durée d'invincibilité de pacman
                        isPacmanBoosted = pm.getSuperPacgum();
                        durationBoostPacman--;
                        if(durationBoostPacman <= 0){
                            pm.setSuperPacgum(false);
                            isPacmanBoosted = false;
                        }
                    }
                    if (e instanceof Fantome && ((Fantome) e).isInJail()){ //Actualisation de la durée de prison de'un fantôme et sa sortie potentielle
                        if(((Fantome) e).decrementTimeJail()){
                            exitJail(((Fantome) e));
                        }
                    }
                }
        		 if(numberOfPacGum <=0){
        		     etat = EtatJeu.FinDeNiveau;
                 }
                
                
                try {
                    Thread.sleep(300); // pause de 0.3s
                } catch (InterruptedException ex) {
                    Logger.getLogger(Pacman.class.getName()).log(Level.SEVERE, null, ex);
                }
        	}

        	setChanged();
            notifyObservers(); // notification de l'observer pour le raffraichisssement graphique

        }
    }

    public boolean isPacmanBoosted() {
        return isPacmanBoosted;
    }

    public EtatJeu getEtat() {
    	return etat;
    }

    public void setEtat(EtatJeu etat) {
        this.etat = etat;
    }

    public int getNiveauActuel() {
        return niveauActuel;
    }
}
