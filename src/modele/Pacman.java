package modele;

public class Pacman extends Entite {

    private int score;
    private boolean hasEatSuperPacgum = false;
    private boolean alreadyHit = false;
    private int hp;

    public Pacman(Jeu _jeu) {
        super(_jeu);
        d = Direction.haut;
        score = 0;
        hp = 3;
    }
    
    public void setDirection(Direction _d) {
        d = _d;
    }

    @Override
    public void choixDirection() {
        
    }
    
    public void scoreup(int bonus) {
    	score += bonus;
    }
    
    public int getScore() {
    	return score;
    }
    
    public int getLife() {
    	return hp;
    }

    public boolean getSuperPacgum() {
        return hasEatSuperPacgum;
    }

    public void setSuperPacgum(boolean pacgum) {
        hasEatSuperPacgum = pacgum;
    }
    
    public void die() {
    	--hp;
    }

    public boolean isAlreadyHit() {
        return alreadyHit;
    }

    public void setAlreadyHit(boolean alreadyHit) {
        this.alreadyHit = alreadyHit;
    }
}
